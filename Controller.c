/* 
 * File:   Controller.c
 * Author: ee475
 *
 * Created on December 12, 2016, 5:58 PM
 */

#include <stdio.h>
#include <stdlib.h>
#include "chess_core.h"
#include "Board.h"
#include "timer_interrupts.c"

void illuminatePieces();
void setMoveablePieces();
void writeBoard();
int boardChanged(uint16_t **prev, struct location *removed);
void updatePrev(uint16_t **prev);

/*
 * 
 */
int main(int argc, char** argv) {

    //timer_init();
    initPIC();
    
    clearLEDs();
    
//    int i;
//    int j;
//    for (i = 0; i <= 7; i++) {
//        for (j = 0; j <= 5; j++) {
//            if (i == j) {
//                greenLED[i][j] = 1;
//            }
//        }
//    }
    uint16_t prevBoard[8][8];
    readBoard();
    updatePrev(prevBoard);
    //setMoveablePieces();
    int hintActive = 1;
    
    while(1) {
        if (hintActive == 1 && turnSwitch == turn) {
            writeBoard();
            continue;
        }

        if (turnSwitch != turn) {
            turn = turnSwitch;
            continue;
        }
        
        readBoard();
        
        struct location removed[1];
        if (boardChanged(prevBoard, removed)) {
            lightUpMoves(removed->row, removed->col, prevBoard[removed->row][removed->col]);
        } else {
            //illuminatePieces();
            setMoveablePieces();
        }
        
        writeBoard();
       
        updatePrev(prevBoard);
        clearLEDs();

    }
    return;
}

int boardChanged(uint16_t **prev, struct location *removed) {
    int row;
    int col;
    for (row = 0; row <= 7; row++) {
        for (col = 0; col <= 7; col++) {
            if (prev[row][col] != board[row][col]) {
                removed->row = row;
                removed->col = col;
                return 1;
            }
        }
    }
    return 0;
}
void writeBoard() {
    int i;
    for (i = 0; i < 100; i++)
        pulseLEDs();
}

void updatePrev(uint16_t **prev) {
    int row = 0;
    int col = 0;
    for (row = 0; row <= 7; row++) {
        for (col = 0; col <= 7; col++) {
            prev[row][col] = board[row][col];
        }
    }
    readBoard();
}

void illuminatePieces() {
    int row;
    int col;
    for (row = 0; row <= 7; row++) {
        for (col = 0; col <= 7; col++) {
            if (board[row][col] > 0)
                blueLED[row][col] = 1;
        }  
    }
}

void setMoveablePieces() {
    int* num;
    struct location pieces[28];
    getMoveablePieces(board, num, pieces);
    for (int i = 0; i < *num; i++) {
        blueLED[pieces[i].row][pieces[i].col] = 1;
    }
    
}

