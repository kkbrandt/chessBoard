/*
 * File:   chess_core.c
 * Author: Kaleo Brandt, Christian Gobrecht
 *
 * Created on December 2, 2016, 10:40 AM
 */
// TODO KALEO: ADD const WHERE APPROPRIATE

//#include <xc.h>
#include <string.h>
#include "Board.h"
#include "chess_core.h"

#define HINT_DEPTH 2

#define abs(x) (x >= 0? x : -x)
//==============================================================================
//                              INDEX OF METHODS
//         ===========================================================
//                                  PUBLIC:
int valueOf(int pieceType);
int getMoveable(uint16_t **board, int row, int col);
int canAttack(uint16_t **board, int ax, int ay, int bx, int by);
int inCheck(uint16_t **board);
void getMoveablePieces(uint16_t **board, int* num, struct location* pieces);
void getMoves(uint16_t **board, int row, int col, int pieceType, int* numLocs, struct location * pieceMoves);
int generateHint(uint16_t **board);
void changeTurn(void);
void delay(int ms);
//         ===========================================================
//                                  PRIVATE:
int isMoveable(int boardValue);
void addLocation(struct location * locs, int row, int col, int* numLocs);
int clearPath(uint16_t **board, int ax, int ay, int bx, int by);
int isEnemy(int boardValue);
int isBlack(int boardValue);
int isWhite(int boardValue);
int generateHintHelper(uint16_t **hintboard, struct location hint[2], int valueMultiplier, int color, int depth);
//==============================================================================
int test = 0;

//==============================================================================
//                              PUBLIC METHODS
int valueOf(int piecetype) {
    if (piecetype == 0)
        return 0;
    if (piecetype == whitePawn || piecetype == blackPawn)
        return 1;
    if (piecetype == whiteKnight || piecetype == blackKnight)
        return 3;
    if (piecetype == whiteBishop || piecetype == blackBishop)
        return 3;
    if (piecetype == whiteRook || piecetype == blackRook)
        return 5;
    if (piecetype == whiteQueen || piecetype == blackQueen)
        return 9;
    
    // Kings are hella valuable.
    return 999;
}

int getMoveable(uint16_t **board, int row, int col) {
    // Can't move an empty space
    if (board[row][col] == empty)
        return 0;
    // If the piece does not belong to the current player, moving it is not allowed.
    if ((turn == WHITE && !isWhite(board[row][col])) || (turn == BLACK && !isBlack(board[row][col])))
        return 0;
    
    struct location pieceMoves[28];
    
    int* numLocs;
    getMoves(board, row, col, board[row][col], numLocs, pieceMoves);
    
    if (*numLocs == 0)
        return 0;
    return 1;
}

// Here "x" refers to row #, and "y" refers to column #.
int canAttack(uint16_t **board, int ax, int ay, int bx, int by) {
    if (ax == bx && ay == by) {
        return 0;
    }
    if (!((isWhite(board[ax][ay]) && isBlack(board[bx][by])) || (isBlack(board[ax][ay]) && isWhite(board[bx][by])))) {
        return 0;
    }
    if (board[ax][ay] == whitePawn){
        if (bx == (ax - 1) && ((by == (ax + 1) || by == (ax - 1))))
            return 1;
        return 0;
    }
    if (board[ax][ay] == blackPawn) {
        if (bx == (ax + 1) && ((by == (ax + 1) || by == (ax - 1))))
            return 1;
        return 0;
    }
    
    int row = 0;
    int col = 0;

    if (board[ax][ay] == whiteKnight || board[ax][ay] == blackKnight) {
        // If the enemy is an L-shape away, knight can attack it.
        if ( (bx == (ax + 1) && by == (ay + 2)) ||
             (bx == (ax + 1) && by == (ay - 2)) ||
             (bx == (ax + 2) && by == (ay + 1)) || 
             (bx == (ax + 2) && by == (ay - 1)) ||
             (bx == (ax - 1) && by == (ay + 2)) ||
             (bx == (ax - 1) && by == (ay - 2)) ||
             (bx == (ax - 2) && by == (ay - 1)) ||
             (bx == (ax - 2) && by == (ay - 1))) {
            return 1;
        }
        return 0;
    }
    if (board[ax][ay] == whiteRook || board[ax][ay] == blackRook) {
        if (!((ay == by) || ax == bx)) {
            return 0;
        }
        // If the enemy is in the same row or column, and all the spaces in between
        // the rook and the enemy are empty, then rook can attack it.
        return clearPath(board, ax, ay, bx, by);
    }
    if (board[ax][ay] == whiteBishop || board[ax][ay] == blackBishop) {
        // If the piece is on a diagonal, then we can move toward it diagonally.
        if (abs(bx-ax) == abs(by-ay)) {
            return clearPath(board, ax, ay, bx, by);
        }
    }
    if (board[ax][ay] == whiteKing || board[ax][ay] == blackKing) {
        if (abs(bx-ax) <= 1 && abs(by-ay) <= 1)
            return 1;
        return 0;
    }
    if (board[ax][ay] == whiteQueen || board[ax][ay] == blackQueen) {
        if ((bx == ax) || (by == ay) || (abs(bx-ax) == abs(by-ay))) {
            return clearPath(board, ax, ay, bx, by);
        }
    }
    return 0;
}

int inCheck(uint16_t **board) {
    int row = 0;
    int col = 0;
    
    // coordinates of king
    int krow = -1;
    int kcol = -1;
   
    // Find the user's king on the board.
    for (row = 0; row < 8; row++) {
        for (col = 0; col < 8; col++) {
           if ((turn == BLACK && board[row][col] == blackKing) || (turn == WHITE && board[row][col] == whiteKing)) {
               krow = row;
               kcol = col;
               break;
            }
        }
    }
    // Check all the pieces on the board to see if any can attack the king.
    for (row = 0; row < 8; row++) {
        for (col = 0; col < 8; col++) {
            if (canAttack(board, row, col, krow, kcol)) {
                return 1;
            }
        }
    }
    return 0;
}

void getMoveablePieces(uint16_t **board, int* num, struct location * pieces) {
    *num = 0;
    int row = 0;
    int col = 0;
    for (row = 0; row <= 7; row++) {
        for (col = 0; col <= 7; col++) {
            if (getMoveable(board, row, col)) {
                addLocation(pieces, row, col, num);
            }
        }
    }
}

// numLocs and pieceMoves are return variables.
void getMoves(uint16_t **board, int row, int col, int pieceType, int* numLocs, struct location * pieceMoves) {
    *numLocs = 0;
    
    if (pieceType == blackPawn) {
        if (row > 0 && board[row-1][col] == empty)
            addLocation(pieceMoves, row-1, col, numLocs);
        if (row > 0 && col > 0 && isWhite(board[row-1][col-1]))
            addLocation(pieceMoves, row-1, col-1, numLocs);
        if (row > 0 && col < 7 && isWhite(board[row-1][col+1]))
            addLocation(pieceMoves, row-1, col+1, numLocs);
    }
    else if (pieceType == whitePawn) {
        if (row <= 6 && board[row+1][col] == empty)
            addLocation(pieceMoves, row+1, col, numLocs);
        if (row <= 6 && col > 0 && isBlack(board[row+1][col-1]))
            addLocation(pieceMoves, row+1, col-1, numLocs);
        if (row <= 6 && col < 7 && isBlack(board[row+1][col+1]))
            addLocation(pieceMoves, row+1, col+1, numLocs);
    }
    else if (pieceType == blackKnight || pieceType == whiteKnight) {
        if (row >= 1 && col >= 2 && isMoveable(board[row-1][col-2]))
            addLocation(pieceMoves, row-1, col-2, numLocs);
        if (row >= 2 && col >= 1 && isMoveable(board[row-2][col-1]))
           addLocation(pieceMoves, row-2, col-1, numLocs);
        if (row >= 2 && col <= 6 && isMoveable(board[row-2][col+1]))
            addLocation(pieceMoves, row-2, col+1, numLocs);
        if (row >= 1 && col <= 5 && isMoveable(board[row-1][col+2]))
            addLocation(pieceMoves, row-1, col+2, numLocs);
        if (row <= 6 && col <= 5 && isMoveable(board[row+1][col+2]))
            addLocation(pieceMoves, row+1, col+2, numLocs);
        if (row <= 5 && col <= 6 && isMoveable(board[row+2][col+1]))
            addLocation(pieceMoves, row+2, col+1, numLocs);
        if (row <= 5 && col >= 1 && isMoveable(board[row+2][col-1]))
            addLocation(pieceMoves, row+2, col-1, numLocs);
        if (row <= 6 && col >= 2 && isMoveable(board[row+1][col-2]))
            addLocation(pieceMoves, row+1, col-2, numLocs);
    }
    else if (pieceType == blackRook || pieceType == whiteRook) {
        int rowT = row;
        int colT = col;
        while (rowT > 0) {
            rowT = rowT - 1;
            if (board[rowT][col] == empty)
                addLocation(pieceMoves, rowT, col, numLocs);
            else if (isEnemy(board[rowT][col])) {
                addLocation(pieceMoves, rowT, col, numLocs);
                break;
            }
        }
        rowT = row;
        colT = col;
        while (rowT < 7) {
            rowT = rowT + 1;
            if (board[rowT][col] == empty)
                addLocation(pieceMoves, rowT, col, numLocs);
            else if (isEnemy(board[rowT][col])) {
                addLocation(pieceMoves, rowT, col, numLocs);
                break;
            }
        }
        rowT = row;
        colT = col;
        while (colT > 0) {
            colT = colT - 1;
            if (board[row][colT] == empty)
                addLocation(pieceMoves, row, colT, numLocs);
            else if (isEnemy(board[row][colT])) {
                addLocation(pieceMoves, row, colT, numLocs);
                break;
            }
        }
        rowT = row;
        colT = col;
        while (colT < 7) {
            colT = colT + 1;
            if (board[row][colT] == empty)
                addLocation(pieceMoves, row, colT, numLocs);
            else if (isEnemy(board[row][colT])) {
                addLocation(pieceMoves, row, colT, numLocs);
                break;
            }
        }
    }
    else if (pieceType == whiteKing || pieceType == blackKing) {
        if (row >= 1 && isMoveable(board[row-1][col]))
            addLocation(pieceMoves, row-1, col, numLocs);
        if (row >= 1 && col >= 1 && isMoveable(board[row-1][col-1]))
            addLocation(pieceMoves, row-1, col-1, numLocs);
        if (col >= 1 && isMoveable(board[row][col-1]))
            addLocation(pieceMoves, row, col-1, numLocs);
        if (row <= 6 && col >=1 && isMoveable(board[row+1][col-1]))
            addLocation(pieceMoves, row+1, col-1, numLocs);
        if (row <= 6 && isMoveable(board[row+1][col]))
            addLocation(pieceMoves, row+1, col, numLocs);
        if (row <= 6 && col <= 6 && isMoveable(board[row+1][col+1]))
            addLocation(pieceMoves, row+1, col+1, numLocs);
        if (col <= 6 && isMoveable(board[row][col+1]))
            addLocation(pieceMoves, row, col+1, numLocs);
        if (row >= 1 && col <=6 && isMoveable(board[row-1][col+1]))
            addLocation(pieceMoves, row-1, col+1, numLocs);
    }
    else if (pieceType == whiteQueen || pieceType == blackQueen) {
        int rowT = row;
        int colT = col;
        
        int rowStep = -1;
        int colStep = -1;
        for (rowStep = -1; rowStep <= 1; rowStep++) {
            for (colStep = -1; colStep <= 1; colStep++) {
                rowT = row;
                colT = col;
                while (rowT >= 0 && rowT <= 7 && colT >= 0 && colT <= 7) {
                    rowT += rowStep;
                    colT += colStep;
                    
                    if (rowT < 0 || rowT > 7 || colT < 0 || colT > 7)
                        break;
                    
                    if (board[rowT][colT] == empty)
                        addLocation(pieceMoves, rowT, colT, numLocs);
                    else if (isEnemy(board[rowT][colT])) {
                        addLocation(pieceMoves, rowT, colT, numLocs);
                        break;
                    }
                }
            }
        }
    }
}

void changeTurn(void) {
    if (turn == WHITE)  {
        turn = BLACK;
    } else {
        turn = WHITE;
    }
}

int generateHint(uint16_t **board) {
    struct location hint[2];
    
    unsigned uint16_t hintboard[8][8];
    int i;
    int j;
    for (i = 0; i <= 7; i++) {
        for (j = 0; j <= 7; j++) {
            hintboard[i][j] = board[i][j];
        }
    }
    
    return generateHintHelper(hintboard, hint, 1, turn, 0);
}

void delay(int ms) {
    int i, j;
    for (i = 0; i < ms; i++) {
        for (j = 0; j < 5; j++) {
            // do nothing! yay!
        }
    }
}

//==============================================================================
//                            PRIVATE HELPERS
void addLocation(struct location * locs, int row, int col, int* numLocs) {
    locs[*numLocs].row = row;
    locs[*numLocs].col = col;
    *numLocs++;
}

// REQUIRES a and b to be diagonal or adjacent to each other.
int clearPath(uint16_t **board, int ax, int ay, int bx, int by) {
    int row = ax;
    int col = ax;
    
    int stepX = ((bx == ax) ? 0 : ((bx > ax) ? 1 : -1));
    int stepY = ((by == ay) ? 0 : ((by > ay) ? 1 : -1));

    while (!(row == bx && col == by)) {
        row += stepX;
        col += stepY;
        if (board[row][col] != empty) {
            return 0;
        }
    }
    return 1;
}

int isMoveable(int boardValue) {
    return ((boardValue == empty) || isEnemy(boardValue));
}

int isEnemy(int boardValue) {
    if (turn == BLACK)
        return isWhite(boardValue);
    else { // It's white's turn.
        return isBlack(boardValue);
    }
}

int isBlack(int boardValue) {
    if (boardValue == whitePawn || boardValue == whiteKnight 
            || boardValue == whiteBishop || boardValue == whiteRook
            || boardValue == whiteQueen || boardValue == whiteKing)
        return 1;
    return 0;
}

int isWhite(int boardValue) {
    if (boardValue == blackPawn || boardValue == blackKnight 
            || boardValue == blackBishop || boardValue == blackRook
            || boardValue == blackQueen || boardValue == blackKing)
        return 1;
    return 0;
}

int generateHintHelper(uint16_t **hintboard, struct location hint[2], int valueMultiplier, int color, int depth) {
    if (depth == HINT_DEPTH) {
        return 0;
    }
    turn = color;
    int opponentColor = ((color == WHITE) ? BLACK : WHITE);
    
    int maxPoints = 0;
    int moveValue = 0;
    
    struct location possibleMoves[28];
    int *numLocs;
    
    int row = 0;
    int col = 0;
    for (row = 0; row <= 7; row++) {
        for (col = 0; col <= 7; col++) {
            if ((turn == WHITE && isWhite(hintboard[row][col])) || (turn == BLACK && isBlack(hintboard[row][col]))) {
                getMoves(hintboard, row, col, hintboard[row][col], numLocs, possibleMoves);
                if (*numLocs > 0) {
                    // -Get possible move locations
                    // -For each one:
                    //   - modify the hintboard
                    //   - calculate the recursive max point value
                    //   - restore the hintboard (undo the move)
                    int i;
                    for (i = 0; i < *numLocs; i++) {
                        struct location dest = possibleMoves[i];
                        // Store the original hintboard pieces so we can reverse the move
                        int pieceType = hintboard[row][col];
                        int takenPieceType = hintboard[dest.row][dest.col];
                        
                        // Move the piece and replace what's already there.
                        hintboard[row][col] = empty;
                        hintboard[dest.row][dest.col] = pieceType;
                        
                        moveValue = valueMultiplier * valueOf(takenPieceType) + generateHintHelper(hintboard, valueMultiplier * -1, opponentColor, hintboard, depth + 1);
                        if (moveValue > maxPoints) {
                            maxPoints = moveValue;
                            // Once we've found the best move to at depth 0, 
                            // that's the move we want to hint.
                            if (depth == 0) {
                                hint[0].row = row;
                                hint[0].col = col;
                                hint[0].row = dest.row;
                                hint[0].col = dest.col;
                            }
                        }
                        hintboard[row][col] = pieceType;
                        hintboard[dest.row][dest.col] = takenPieceType;
                    }
                }
            }
        }
    }
    turn = color; 
    return maxPoints * valueMultiplier;
}

/*
void readBoardRow(int row) {
    // If a tile's value has transitioned, record the transition
    pieceCLK = 1;
    pieceCLK = 0;
    if (temp_board[row][4] != registerData)
        board[row][0]++;
    pieceCLK = 1;
    pieceCLK = 0;
    if (temp_board[row][5] != registerData)
        board[row][1]++;
    pieceCLK = 1;
    pieceCLK = 0;
    if (temp_board[row][6] != registerData)
        board[row][2]++;
    pieceCLK = 1;
    pieceCLK = 0;
    if (temp_board[row][7] != registerData)
        board[row][3]++;
    pieceCLK = 1;
    pieceCLK = 0;
    if (temp_board[row][0] != registerData)
        board[row][4]++;
    pieceCLK = 1;
    pieceCLK = 0;
    if (temp_board[row][1] != registerData)
        board[row][5]++;
    pieceCLK = 1;
    pieceCLK = 0;
    if (temp_board[row][2] != registerData)
        board[row][6]++;
    pieceCLK = 1;
    pieceCLK = 0;
    if (temp_board[row][3] != registerData)
        board[row][7]++;

    // Update current tile values
    temp_board[row][0] = register0Data;
    temp_board[row][1] = register1Data;
    temp_board[row][2] = register2Data;
    temp_board[row][3] = register3Data;
    temp_board[row][4] = register4Data;
    temp_board[row][5] = register5Data;
    temp_board[row][6] = register6Data;
    temp_board[row][7] = register7Data;
}
*/

//int getMoveable(int row, int col) {
//    // Can't move an empty space
//    if (board[row][col] == empty)
//        return 0;
//    
//    // Handle pawns first, as they can only move in 1 direction and move differently for move/attack.
//    if (turn == BLACK && board[row][col] == blackPawn) {
//        // Black moves toward lower-numbered rows
//        // If there's an empty space ahead, or an enemy to the diagonal, then pawn can move.
//        if ((row > 0 && board[row-1][col] == empty) 
//             || row > 0 && col > 0 && isWhite(board[row-1][col-1])
//             || row > 0 && col < 7 && isWhite(board[row-1][col+1]))
//            return 1;
//        return 0;
//    }
//    if (turn == WHITE && board[row][col] == whitePawn) {
//        // White moves toward higher-numbered rows
//        // If there's an empty space ahead, or an enemy to the diagonal, then pawn can move.
//        if ((row <= 6 && board[row+1][col] == empty) 
//             || row <= 6 && col > 0 && isBlack(board[row+1][col-1])
//             || row <= 6 && col < 7 && isBlack(board[row+1][col+1]))
//            return 1;
//        return 0;
//    }
//    if (board[row][col] == blackKnight || board[row][col] == whiteKnight) {
//        // if there's an empty space or enemy in any of the 8 L-moves, then knight can move.
//        if ((row >= 1 && col >= 2 && isMoveable(board[row-1][col-2]))
//             || (row >= 2 && col >= 1 && isMoveable(board[row-2][col-1]))
//             || (row >= 2 && col <= 6 && isMoveable(board[row-2][col+1]))
//             || (row >= 1 && col <= 5 && isMoveable(board[row-1][col+2]))
//             || (row <= 6 && col <= 5 && isMoveable(board[row+1][col+2]))
//             || (row <= 5 && col <= 6 && isMoveable(board[row+2][col+1]))
//             || (row <= 5 && col >= 1 && isMoveable(board[row+2][col-1]))
//             || (row <= 6 && col >= 2 && isMoveable(board[row+1][col-2])))
//            return 1;
//        return 0;
//    }
//    if (board[row][col] == blackRook || board[row][col] == whiteRook) {
//        // if there's an empty space or enemy in any of the 4 non-diagonal locations, then rook can move.
//        if (row >= 1 && isMoveable(board[row-1][col])
//             || col >= 1 && isMoveable(board[row][col-1])
//             || row <= 6 && isMoveable(board[row+1][col])
//             || col <= 6 && isMoveable(board[row][col+1]))
//            return 1;
//        return 0;
//    }
//    if (board[row][col] == whiteBishop || board[row][col] == blackBishop) {
//        // If there's an empty space or enemy in any of the 4 diagonal locations, then bishop can move.
//        if ((row >= 1 && col >= 1 && isMoveable(board[row-1][col-1]))
//             || (row >= 1 && col <= 6 && isMoveable(board[row-1][col+1]))
//             || (row <= 6 && col >= 1 && isMoveable(board[row+1][col-1]))
//             || (row <= 6 && col <= 6 && isMoveable(board[row+1][col+1])))
//            return 1;
//        return 0;
//    }
//    if (board[row][col] == blackQueen || board[row][col] == whiteQueen 
//            || board[row][col] == blackKing || board[row][col] == whiteKing) {
//        // if there's an empty space or enemy in any of the 8 adjacent locations, then queen or king can move.
//        if (row >= 1 && isMoveable(board[row-1][col])
//             || row >= 1 && col >= 1 && isMoveable(board[row-1][col-1])
//             || col >= 1 && isMoveable(board[row][col-1])
//             || row <= 6 && col >=1 && isMoveable(board[row+1][col-1])
//             || row <= 6 && isMoveable(board[row+1][col])
//             || row <= 6 && col <= 6 && isMoveable(board[row+1][col+1])
//             || col <= 6 && isMoveable(board[row][col+1])
//             || row >= 1 && col <=6 && isMoveable(board[row-1][col+1]))
//            return 1;
//        return 0;
//    }
//    
//    return 0;  // null case; should never happen.
//}

//#define register0Data PORTCbits.RC0
//#define register1Data PORTCbits.RC1
//#define register2Data PORTCbits.RC2
//#define register3Data PORTCbits.RC3
//#define register4Data PORTCbits.RC4
//#define register5Data PORTCbits.RC5
//#define register6Data PORTCbits.RC6
//#define register7Data PORTCbits.RC7

//#define LEDRow0En_L somepin
//#define LEDRow1En_L somepin
//#define LEDRow2En_L somepin
//#define LEDRow3En_L somepin
//#define LEDRow4En_L somepin
//#define LEDRow5En_L somepin
//#define LEDRow6En_L somepin
//#define LEDRow7En_L somepin