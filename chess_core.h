/* 
 * File:   chess_core.h
 * Author: ee475
 *
 * Created on December 12, 2016, 5:49 PM
 */

#ifndef CHESS_CORE_H
#define	CHESS_CORE_H

#ifdef	__cplusplus
extern "C" {
#endif
    
#include <stdint.h>

//=========== These values correspond to Table 1 in the design doc. ============
// Each value is the number of times the piece will be asserted 
// when clocked 4096 times.
#define empty 0
#define whitePawn 2048
#define blackPawn 1024
#define whiteKnight 512
#define blackKnight 256
#define whiteBishop 128
#define blackBishop 64
#define whiteRook 32
#define blackRook 16
#define whiteQueen 8
#define blackQueen 4
#define whiteKing 2
#define blackKing 1

struct location {
    int row;
    int col;
};

int turn = 0; // 0=white, 1=black.
#define WHITE 0
#define BLACK 1

int valueOf(int pieceType);
int getMoveable(uint16_t **board, int row, int col);
int canAttack(uint16_t **board, int ax, int ay, int bx, int by);
int inCheck(uint16_t **board);
void getMoveablePieces(uint16_t **board, int* num, struct location* pieces);
void getMoves(uint16_t **board, int row, int col, int pieceType, int* numLocs, struct location * pieceMoves);
int generateHint(uint16_t **board);
void changeTurn(void);
void delay(int ms);

#ifdef	__cplusplus
}
#endif

#endif	/* CHESS_CORE_H */

