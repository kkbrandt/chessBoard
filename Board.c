/*
 * File:   Board.c
 * Author: ee475
 *
 * Created on December 12, 2016, 5:44 PM
 */


#include <xc.h>
#include <string.h>
#include "Board.h"

//==============================================================================
//                              PIN DEFINITIONS
#define pieceCLK LATAbits.LATA0

#define registerSh_Ld LATCbits.LATC2
#define registerCLK LATCbits.LATC3

#define registerData PORTCbits.RC4

// Enable signal for a row of LEDs is active-low
#define LEDRowEnReg LATBbits.LATB1
#define LEDRowEnClk LATBbits.LATB0

#define LEDCol0 LATBbits.LATB2
#define LEDCol1 LATBbits.LATB3
#define LEDCol2 LATBbits.LATB4
#define LEDCol3 LATBbits.LATB5
#define LEDCol4 LATCbits.LATC1
#define LEDCol5 LATCbits.LATC0
#define LEDCol6 LATAbits.LATA6
#define LEDCol7 LATAbits.LATA5

#define LEDRED LATCbits.LATC5
#define LEDGREEN LATCbits.LATC6
#define LEDBLUE LATCbits.LATC7

//==============================================================================
//                               GLOBAL VARS
unsigned uint16_t temp_board[8][8];

//==============================================================================
//                              INDEX OF METHODS
//         ===========================================================
//                                  PUBLIC:
void initPIC();
void clearLEDs(void);
void pulseLEDs(void);
void lightUpMoves(int row, int col, int pieceType);
void readBoard(void);

//         ===========================================================
//                                  PRIVATE:
void clearBoard(void);
void pulseLEDColor(unsigned uint16_t boardData[8][8]);
void readBoardRow(int row);
void loadLEDRow(unsigned uint16_t boardData[8][8], int row);
void pulseRow(int row);

//==============================================================================
//                              PUBLIC METHODS
void initPIC() {
    // (TODO confirm correct) Use external clock
    OSCTUNEbits.INTSRC = 1;
    OSCCON2bits.MFIOSEL = 0;
    OSCCONbits.IDLEN = 1;
    OSCCONbits.IRCF = 0;
    OSCCONbits.OSTS = 1;
    OSCCONbits.HFIOFS = 0;
    OSCCONbits.SCS = 0;
    
    SSP2STAT = 0x00;
    SSP2CON1 = 0x00;
    SSP2CON1bits.SSPEN = 0;  // enable synchronous serial port

    ANSELA = 0;
    ANSELB = 0;
    ANSELC = 0;
    
    //  configure pins as I/O
    // 0's are *output*
    TRISA = 0b00000000;
    TRISB = 0b00000000;
    TRISC = 0b00010000;
    
    // registers should load.
    registerSh_Ld = 0;
    
    // empty LED row enable data
    LEDRowEnReg = 1;
    int i = 0;
    for (i = 0; i < 8; i++) {
        LEDRowEnClk = 1;
        LEDRowEnClk = 0;
    }
   
}

void clearBoard(void) {
    int row = 0;
    int col = 0;
    for (row = 0; row < 8; row++) {
        for (col = 0; col < 8; col++) {
            board[row][col] = 0;
            temp_board[row][col] = 0;
        }    
    }
}
void clearLEDs(void) {
    int row = 0;
    int col = 0;
    for (row = 0; row < 8; row++) {
        for (col = 0; col < 8; col++) {
            redLED[row][col] = 0;
            greenLED[row][col] = 0;
            blueLED[row][col] = 0;
        }    
    }
    redLED[2][2] = 1;
    redLED[5][5] = 1;
    redLED[2][5] = 1;
    redLED[5][2] = 1;
    greenLED[1][1] = 1;
    greenLED[1][6] = 1;
    greenLED[6][6] = 1;
    greenLED[6][1] = 1;
}

void pulseLEDs(void) {
    LEDRED = 0;
    LEDGREEN = 0;
    LEDBLUE = 0;
    // Load a single 0 (enable) into the row enable register
    LEDRowEnReg = 0;
    
    LEDRowEnClk = 1;
    pulseRow(0);
    LEDRowEnClk = 0;
    
    LEDRowEnReg = 1;
    
    for (int i = 1; i <= 7; i++) {
        LEDRowEnClk = 1;
        pulseRow(i);
        LEDRowEnClk = 0;
        
    }
}

// light up the moves that a piece picked up from [row][col] is able to make.
// TODO: Include logic that disallows moves that put the player's king in check.
void lightUpMoves(int row, int col, int pieceType) {
    clearLEDs();
    
    struct location pieceMoves[28];
    
    int* numLocs = 0;
    getMoves(board, row, col, pieceType, numLocs, pieceMoves);
    
    int i = 0;
    for (i = 0; i < *numLocs; i++) {
        greenLED[pieceMoves[i].row][pieceMoves[i].col] = 1;
    }
}

// After execution, board will store number of transitions on each tile over full cycle
void readBoard(void) {
    int i = 0;
    int row = 0;
    
    // Reset board 
    clearBoard();
    clearLEDs();
    
    for (i = 0; i < 4096; i++) {
        // clock all the board pieces
        pieceCLK = 1;
        delay(1);
        pieceCLK = 0;
        
        // now, shift out the inputs.
        registerSh_Ld = 1;
        
        // For each bit in the 8-bit shift registers:
        readBoardRow(0);
        readBoardRow(1);
        readBoardRow(2);
        readBoardRow(3);
        readBoardRow(4);
        readBoardRow(5);
        readBoardRow(6);
        readBoardRow(7);
//        readBoardRow(0);
//        readBoardRow(3);
//        readBoardRow(1);
//        readBoardRow(2);
//        readBoardRow(4);
//        readBoardRow(7);
//        readBoardRow(5);
//        readBoardRow(6);
        
        // switch back to parallel load.
        registerSh_Ld = 0;
    }
}
//==============================================================================
//                            PRIVATE HELPERS

void pulseRow(int row) {
    loadLEDRow(redLED, row);
    LEDRED = 1;
    delay(100);
    LEDRED = 0;
    
    loadLEDRow(blueLED, row);
    LEDBLUE = 1;
    delay(100);
    LEDBLUE = 0;
    
    loadLEDRow(greenLED, row);
    LEDGREEN = 1;
    delay(100);
    LEDGREEN = 0;
}

/** Load a row of the LED matrix array into the actual board */
void loadLEDRow(unsigned uint16_t boardData[8][8], int row) {
    LEDCol0 = boardData[row][0];
    LEDCol1 = boardData[row][1];
    LEDCol2 = boardData[row][2];
    LEDCol3 = boardData[row][3];
    LEDCol4 = boardData[row][4];
    LEDCol5 = boardData[row][5];
    LEDCol6 = boardData[row][6];
    LEDCol7 = boardData[row][7];
}

void readBoardRow(int row){
    int i;
    
    for(i = 3; i >= 0; i--) {
        registerCLK = 1;
        registerCLK = 0;
        if (temp_board[row][i] != registerData) {
            board[row][i] += registerData;
            //greenLED[row][i] = 1;
        }
        
        temp_board[row][i] = registerData;
    }
    for(i = 7; i >= 4; i--) {
        registerCLK = 1;
        registerCLK = 0;
        if (temp_board[row][i] != registerData) {
            board[row][i] += registerData;
            //greenLED[row][i] = 1;
        }
        
        temp_board[row][i] = registerData;
    }
}