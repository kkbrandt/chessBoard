/* 
 * File:   Board.h
 * Author: ee475
 *
 * Created on December 12, 2016, 5:46 PM
 */

#ifndef BOARD_H
#define	BOARD_H

#ifdef	__cplusplus
extern "C" {
#endif
    
#include "chess_core.h"
#include <p18f25k22.h>
    
#define turnSwitch LATAbits.LATA1
#define hintBtn LATAbits.LATA2
    
void initPIC();
/** Clear the board's LEDs */
void clearLEDs(void);
/** Pulse the whole board of LEDs.*/
void pulseLEDs(void);
/** Light up the moves that a piece picked up from [row][col] is able to make.
 *  TODO: Include logic that disallows moves that put the player's king in check.
 */
void lightUpMoves(int row, int col, int pieceType);
/** Reads the board into the board datas.*/
void readBoard(void);

//==============================================================================
//                               GLOBAL VARS
unsigned uint16_t board[8][8];
unsigned uint16_t redLED[8][8];
unsigned uint16_t blueLED[8][8];
unsigned uint16_t greenLED[8][8];

#ifdef	__cplusplus
}
#endif

#endif	/* BOARD_H */

